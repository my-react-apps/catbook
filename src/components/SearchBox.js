import React from 'react'

const SearchBox = ({ searchField, searchChange }) => {
  return (
    <div className="search-box">
      <input
        className="seachbox__input"
        type="search"
        placeholder="search by name"
        onChange={searchChange}
      />
    </div>
  );
}

export default SearchBox;
