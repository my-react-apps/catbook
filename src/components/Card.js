import React from "react";

const Card = ({ id, name, email, random }) => {
  return (
    <div className="card__container">
      <img 
        className="card__img"
        src={`https://robohash.org/set_set4/bgset_bg1/${id}?size=200x200`}
        alt="profile"
      />
      <div className="card__info">
        <h2 className="info__name">{ name }</h2>
        <p className="info__email">{ email }</p>
      </div>
    </div>
  );
};

export default Card;
