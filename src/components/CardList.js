import React from "react";
import Card from "./Card";

const CardList = ({ cats, random }) => {
  return (
    <div className="card-list--container">
      {cats.map((cat, index) => {
        return (
          <Card
            key={index}
            id={cats[index].id}
            name={cats[index].name}
            email={cats[index].email}
          />
        );
      })}
    </div>
  );
};

export default CardList;
