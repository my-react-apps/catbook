import React, { Component } from "react";
import { connect } from 'react-redux';
import { setSearchField, requestCats } from '../actions';

import CardList from "../components/CardList";
import SearchBox from "../components/SearchBox";
import ErrorBoundary from "../components/ErrorBoundary";

const mapStateToProps = state => {
  return {
    searchField: state.searchCats.searchField,
    cats: state.requestCats.cats,
    isPending: state.requestCats.isPending,
    error: state.requestCats.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSearchChange: event => dispatch(setSearchField(event.target.value)),
    onRequestCats: () => dispatch(requestCats())
  };
}

class App extends Component {
  componentDidMount() {
    this.props.onRequestCats();
  }

  render() {
    const { searchField, onSearchChange, cats, isPending } = this.props;

    const filteredCats = cats.filter(cat => {
      return cat.name.toLowerCase().includes(searchField.toLowerCase());
    });

    return isPending ? (
      <h1 className="page--loading">Loading...</h1>
    ) : (
      <div className="page">
        <div className="page__header">
          <h1 className="page__title">Catbook</h1>
          <p className="page__desc">
            Simple react app I created while learing react. It uses two different APIs to fetch images, names and emails, and than display them.
          </p>
          <p className="page__desc">
            You can search cats by name!
          </p>
          <SearchBox className="page__search" searchChange={onSearchChange} />
        </div>
        <ErrorBoundary>
          <CardList cats={filteredCats}/>
        </ErrorBoundary>
        <div className="page__footer">
          <p className="footer__text">
            Cats lovingly delivered by
            <a className="footer__link" href="https://robohash.org/" target="_blank" rel="noopener noreferrer"> Robohash.org</a>
          </p>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
