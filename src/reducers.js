import {
  CHANGE_SEARCH_FIELD,
  REQUEST_CATS_PENDING,
  REQUEST_CATS_SUCCESS,
  REQUEST_CATS_FAILED
} from './constants';

const initialStateSearch = {
  searchField: ""
}

const initialStateCats = {
  isPending: false,
  cats: [],
  error: ''
}

// Set default values for parameters so that we can leave them empty without error.
export const searchCats = (state=initialStateSearch, action={}) => {
  switch (action.type) {
    case CHANGE_SEARCH_FIELD:
      // We are returning new state - '{}' 
      // that will have everything in the state - 'state'
      // than update searchField prop with action.payload -
      // '{ searchField: action.payload }'
      // We can also use ES8: '{ ...state, { searchField: action.payload }'
      return Object.assign({}, state, { searchField: action.payload });

    default:
      return state;
  }
}

export const requestCats = (state=initialStateCats, action={}) => {
  switch (action.type) {
    case REQUEST_CATS_PENDING:
      return Object.assign({}, state, { isPending: true });

    case REQUEST_CATS_SUCCESS:
      return Object.assign({}, state, { cats: action.payload, isPending: false });

    case REQUEST_CATS_FAILED:
      return Object.assign({}, state, { erorr: action.payload, isPending: false });

    default:
      return state;
  }
}
